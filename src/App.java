
public class App {
    public static void main(String[] args) throws Exception {
        Rectangle rectangle1 = new Rectangle();
        System.out.println("get Area:" + rectangle1.getArea());
        System.out.println("get Perimeter:" + rectangle1.getPerimeter());

        Rectangle rectangle2 = new Rectangle(3.0f, 2.0f);
        System.out.println("get Area:" + rectangle2.getArea());
        System.out.println("get Perimeter:" + rectangle2.getPerimeter());
    }

}